
# POC User Registration API 
This POC is to register and find users.

### Prerequisites
* Java 17 or later
* Maven 3.8.4 or later
* Docker(optional)

### Installation
* 1 - Clone repository 
```
  git clone git@gitlab.com:addou.elhoucine/userapi-project.git
```
* 2 - Navigate to the root project directory :
```
  cd userapi-project
```

* 3 - Build the project using Maven :
```
  mvn clean install
```
* 4 - Run the project :
```
  mvn spring-boot:run
```
* 5 - Open the application in web browser by navigating to `http://localhost:8080`

### Configuration
You can configure the application by editing the `application.properties` file located in `src/main/resources` directory.
The file contains properties such as server port, database connection settings, and others.

### Database
The application is H2 database by default. You can change database settings in the `application.properties`.

### Documentation
The application's API is available at `http://localhost:8080/api/swagger-ui/index.html`.

The full API documentation is available in the directory `doc`.

### Code style
In this project, we follow the Google Java Style Guide for code formatting and style. 
We recommend to use check-style file in `checkstyle` directory