package com.company.poc.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import com.company.poc.repositories.entities.UserEntity;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryTest {


  @Autowired
  private UserRepository userRepository;

  @Test
  public void whenFindByName_thenReturnUser() {
    // given
    UserEntity alex = new UserEntity();
    alex.setName("Bruno");
    alex.setBirthDate(LocalDate.parse("2002-10-11"));
    alex.setCountryOfResidence("france");
    alex.setPhoneNumber("0611111111");
    alex.setGender("Mr");
    userRepository.save(alex);

    // when
    Optional<UserEntity> user = userRepository.findByName("Bruno");

    // then
    assertThat(user.isPresent()).isTrue();
    assertThat(user.get().getName())
        .isEqualTo(alex.getName());
  }

  @Test
  public void whenFindByName_thenReturnEmpty() {
    assertThat(userRepository.findByName("Jon").isEmpty()).isTrue();
  }
}


