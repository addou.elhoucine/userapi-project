package com.company.poc.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.company.poc.dto.User;
import com.company.poc.exceptions.DuplicatedUserNameException;
import com.company.poc.repositories.UserRepository;
import com.company.poc.repositories.entities.UserEntity;
import com.company.poc.services.mapper.UserMapper;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private UserMapper userMapper;

  @InjectMocks
  private UserServiceImpl userService;
  private User user;
  private UserEntity userEntity;

  @BeforeEach
  void setUp() {
    user = new User();
    user.setName("Bruno");
    user.setBirthDate(LocalDate.parse("2002-10-11"));
    user.setCountryOfResidence("france");
    user.setPhoneNumber("0611111111");
    user.setGender("Mr");

    userEntity = new UserEntity();
    userEntity.setName("Bruno");
    userEntity.setBirthDate(LocalDate.parse("2002-10-11"));
    userEntity.setCountryOfResidence("france");
    userEntity.setPhoneNumber("0611111111");
    userEntity.setGender("Mr");
  }

  @Test
  public void testRegister_shouldRegisterUser() {
    // given
    when(userRepository.findByName(user.getName())).thenReturn(Optional.empty());
    when(userMapper.modelToEntity(user)).thenReturn(userEntity);
    when(userRepository.save(userEntity)).thenReturn(userEntity);
    when(userMapper.entityToModel(userEntity)).thenReturn(user);

    // when
    User result = userService.register(user);

    // then
    assertEquals(user, result);
    verify(userRepository).save(userEntity);
    verify(userMapper).modelToEntity(user);
    verify(userMapper).entityToModel(userEntity);
  }

  @Test
  public void testRegister_shouldThrowDuplicatedUserNameException() {
    // given
    when(userRepository.findByName(user.getName())).thenReturn(Optional.ofNullable(userEntity));

    // when / then
    assertThrows(DuplicatedUserNameException.class, () -> userService.register(user));
  }

  @Test
  public void testFindByName_shouldReturnUser() {
    // given
    String name = userEntity.getName();
    when(userRepository.findByName(name)).thenReturn(Optional.ofNullable(userEntity));
    when(userMapper.entityToModel(userEntity)).thenReturn(user);

    // when
    Optional<User> result = userService.findByName(name);

    // then
    assertTrue(result.isPresent());
    assertEquals(user, result.get());
    verify(userRepository).findByName(name);
    verify(userMapper).entityToModel(userEntity);
  }
}