package com.company.poc.dto;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ResponseErrorHandler {

  private String message;
  private HttpStatus status;
  public LocalDateTime timeStamp;

  public ResponseEntity<Object> generateResponse() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("message", this.message);
    map.put("status", this.status.value());
    map.put("timeStamp", timeStamp);
    return new ResponseEntity<Object>(map, this.status);
  }


}
