package com.company.poc.dto;


import com.company.poc.apis.Validator.Adult;
import com.company.poc.apis.Validator.ValidResidence;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {

  private Long id;
  @NotNull(message = "username attribute is required")
  private String name;

  @Adult
  @NotNull(message = "birthDate attribute is required")
  private LocalDate birthDate;

  @ValidResidence
  @NotNull(message = "countryOfResidence attribute is required")
  private String countryOfResidence;
  @Pattern(regexp = ("^(?:(?:\\+|00)33|0)\\s*[1-9](?:[\\s.-]*\\d{2}){4}$"), message = "please provide a valid french phone number")
  private String phoneNumber;

  private String gender;


}
