package com.company.poc.services;

import com.company.poc.dto.User;
import java.util.Optional;

/**
 * UserService interface
 */
public interface UserService {

  /**
   * register a new user
   *
   * @param user : user to registered
   * @return registered user
   */
  User register(User user);

  /**
   * @param name
   * @return
   */
  Optional<User> findByName(String name);
}
