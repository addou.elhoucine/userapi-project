package com.company.poc.services.impl;

import static java.lang.String.format;

import com.company.poc.dto.User;
import com.company.poc.exceptions.DuplicatedUserNameException;
import com.company.poc.repositories.UserRepository;
import com.company.poc.services.UserService;
import com.company.poc.services.mapper.UserMapper;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final UserMapper userMapper;

  @Autowired
  public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
    this.userRepository = userRepository;
    this.userMapper = userMapper;
  }

  @Override
  public User register(User user) {
    userRepository.findByName(user.getName())
        .ifPresent(userEntity1 -> {
          throw new DuplicatedUserNameException(
              format("The user name '%s' already exist", user.getName()));
        });

    return userMapper.entityToModel(userRepository.save(userMapper.modelToEntity(user)));

  }

  @Override
  public Optional<User> findByName(String name) {
    return userRepository.findByName(name)
        .map(userMapper::entityToModel);
  }
}
