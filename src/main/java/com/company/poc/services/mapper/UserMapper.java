package com.company.poc.services.mapper;

import com.company.poc.dto.User;
import com.company.poc.repositories.entities.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserMapper {

  User entityToModel(UserEntity userEntity);

  UserEntity modelToEntity(User user);
}
