package com.company.poc.apis;

import com.company.poc.dto.User;
import com.company.poc.exceptions.DataNotFoundException;
import com.company.poc.monitoring.Audit;
import com.company.poc.repositories.UserRepository;
import com.company.poc.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("v1/users")
public class UserApi {

  private final UserService userService;
  private final UserRepository userRepository;

  @Autowired
  public UserApi(UserService userService,
      UserRepository userRepository) {
    this.userService = userService;
    this.userRepository = userRepository;
  }

  @Operation(summary = "Create new user")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "user has been successfully created"),
      @ApiResponse(responseCode = "400", description = "Required parameter is not present or not valid"),
  })
  @Audit
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<User> createUser(@RequestBody @Valid User newUserRequest) {
    var user = userService.register(newUserRequest);
    URI userLocation = ServletUriComponentsBuilder
        .fromCurrentRequest()
        .path("/{id}")
        .buildAndExpand(user.getId())
        .toUri();
    return ResponseEntity.created(userLocation).build();

  }

  @Operation(summary = "Get user by name")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "The user is found"),
      @ApiResponse(responseCode = "404", description = "No user found with the specific name"),
      @ApiResponse(responseCode = "400", description = "Required request parameter is not present"),
  })
  @Audit
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<User> getUser(@RequestParam String name) {
    return userService.findByName(name)
        .map(ResponseEntity::ok)
        .orElseThrow(() -> new DataNotFoundException("User name not found"));
  }
}
