package com.company.poc.apis.Validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ResidenceValidator.class)
public @interface ValidResidence {

  String message() default "Invalid residence country, only French user can register";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}