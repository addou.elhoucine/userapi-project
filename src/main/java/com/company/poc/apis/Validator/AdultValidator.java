package com.company.poc.apis.Validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;

public class AdultValidator implements ConstraintValidator<Adult, LocalDate> {

  @Override
  public void initialize(Adult constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(LocalDate birthDate,
      ConstraintValidatorContext constraintValidatorContext) {
    if (birthDate == null) {
      return true;
    }
    LocalDate today = LocalDate.now();
    Period age = Period.between(birthDate, today);
    return age.getYears() >= 18;
  }
}
