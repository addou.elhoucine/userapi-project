package com.company.poc.apis.Validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Value;

public class ResidenceValidator implements ConstraintValidator<ValidResidence, String> {

  @Value("${residence.country}")
  private String country;

  @Override
  public boolean isValid(String residence, ConstraintValidatorContext context) {
    if (residence == null) {
      return true;
    }
    return residence.equalsIgnoreCase(country);
  }
}
