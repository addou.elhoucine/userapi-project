package com.company.poc.monitoring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuditAdvice {

  Logger logger = LoggerFactory.getLogger(AuditAdvice.class);

  @Around("@annotation(com.company.poc.monitoring.Audit)")
  public Object trackTime(ProceedingJoinPoint pjp) throws Throwable {
    long startTime = System.currentTimeMillis();
    Object obj = pjp.proceed();
    long endTime = System.currentTimeMillis();
    logger.info(
        "Method name : " + pjp.getSignature() + " Processing time : " + (endTime - startTime)
            + "(ms)");
    return obj;
  }
}
