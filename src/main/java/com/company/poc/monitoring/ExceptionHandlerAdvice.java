package com.company.poc.monitoring;


import com.company.poc.dto.ResponseErrorHandler;
import com.company.poc.exceptions.DataNotFoundException;
import com.company.poc.exceptions.DuplicatedUserNameException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerAdvice {

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Object> handleException(MethodArgumentNotValidException exception) {
    return new ResponseErrorHandler(
        exception.getBindingResult().getFieldErrors()
            .stream().map(error -> error.getDefaultMessage())
            .collect(Collectors.joining(", "))
        , HttpStatus.BAD_REQUEST, LocalDateTime.now()).generateResponse();
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MissingServletRequestParameterException.class)
  public ResponseEntity<Object> handleException(MissingServletRequestParameterException exception) {
    return new ResponseErrorHandler(
        exception.getMessage()
        , HttpStatus.BAD_REQUEST, LocalDateTime.now()).generateResponse();
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(DateTimeParseException.class)
  public ResponseEntity<Object> handleException(DateTimeParseException exception) {
    return new ResponseErrorHandler(
        "birthDate attribute should respect format yyyy-mm-dd"
        , HttpStatus.BAD_REQUEST, LocalDateTime.now()).generateResponse();
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(DuplicatedUserNameException.class)
  public ResponseEntity<Object> handleException(DuplicatedUserNameException exception) {
    return new ResponseErrorHandler("user name already exist", HttpStatus.BAD_REQUEST,
        LocalDateTime.now()).generateResponse();
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(DataNotFoundException.class)
  public ResponseEntity<Object> handleException(DataNotFoundException exception) {
    return new ResponseErrorHandler("user name not found", HttpStatus.NOT_FOUND,
        LocalDateTime.now()).generateResponse();
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleException(Exception ex) {
    return new ResponseErrorHandler(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
        LocalDateTime.now()).generateResponse();
  }

}
