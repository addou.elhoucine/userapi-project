package com.company.poc.monitoring;

import com.company.poc.exceptions.FunctionalException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAdvice {

  Logger logger = LoggerFactory.getLogger(LoggingAdvice.class);

  @Pointcut(value = "execution(* com.company.poc.apis.UserApi.*(..) )")
  public void apiPointCut() {
  }

  @Around("apiPointCut()")
  public Object applicationLogger(ProceedingJoinPoint pjp) throws Throwable {
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());

    String methodName = pjp.getSignature().getName();
    String className = pjp.getTarget().getClass().toString();
    logger.info("Start method: {}.{} with args: {} ", className, methodName, pjp.getArgs());
    Object result = null;
    try {
      result = pjp.proceed();
    } catch (FunctionalException dnf) {
      logger.warn(dnf.getMessage());
      throw dnf;
    } catch (Throwable e) {
      logger.error("Error while calling method {}.{}", className, methodName, e);
      throw e;
    } finally {
      logger.info("End method: {}.{} with response: {} ", className, methodName, result);
    }
    return result;

  }
}
