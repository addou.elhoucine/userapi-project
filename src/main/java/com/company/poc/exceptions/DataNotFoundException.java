package com.company.poc.exceptions;

public class DataNotFoundException extends FunctionalException {

  public DataNotFoundException() {
    super();
  }

  public DataNotFoundException(String message) {
    super(message);
  }
}
