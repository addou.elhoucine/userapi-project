package com.company.poc.exceptions;

public class DuplicatedUserNameException extends FunctionalException {

  public DuplicatedUserNameException() {
    super();
  }

  public DuplicatedUserNameException(String message) {
    super(message);
  }
}
