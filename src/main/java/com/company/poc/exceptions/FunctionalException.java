package com.company.poc.exceptions;

public class FunctionalException extends RuntimeException {

  public FunctionalException() {
    super();
  }

  public FunctionalException(String message) {
    super(message);
  }
}
